package pl.loando.sportnewsmanager.interfaces;

public interface BadgeListener {
    void setBadgeValue(int type);
}
