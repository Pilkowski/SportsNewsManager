package pl.loando.sportnewsmanager.interfaces;

public interface ItemTouchHelperListener {

    boolean onItemMove(int fromPosition, int toPosition);

}
