package pl.loando.sportnewsmanager.interfaces;

import android.app.Activity;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import pl.loando.sportnewsmanager.navigation.Navigator;

public interface Providers {
    Navigator getNavigator();
    Activity getActivity();
    FragmentManager getManager();
    ViewDataBinding getBinding();
    ViewDataBinding getFragmentBinding();
}
