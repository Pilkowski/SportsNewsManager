package pl.loando.sportnewsmanager.activities.addnews;

import android.app.Activity;
import android.content.Intent;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.base.BaseActivity;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.ActivityAddNewsBinding;
import pl.loando.sportnewsmanager.helpers.TmpDataBase;
import pl.loando.sportnewsmanager.interfaces.Providers;
import pl.loando.sportnewsmanager.models.ChooserItem;
import pl.loando.sportnewsmanager.models.UserPreferences;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class AddNewsActivity extends BaseActivity<ActivityAddNewsBinding, AddNewsViewModel> implements Providers {


    @Override
    protected void initActivity(ActivityAddNewsBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        UserPreferences.init(UserPreferences.SETTINGS, getApplicationContext());
        navigator.openNewsChooser();
        setSupportActionBar(binding.addNewsToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TmpDataBase.init();
        List<ChooserItem> list1 = new ArrayList<>();
        list1.add(new ChooserItem("Piłka nożna", "https://cdn1.iconfinder.com/data/icons/education-259/100/education-19-512.png", ChooserItem.DISCIPLINE,
                false, 2));
        list1.add(new ChooserItem("Siatkówka", "https://cdn4.iconfinder.com/data/icons/sports-38/58/volei-512.png",
                ChooserItem.DISCIPLINE, false, 3));
        TmpDataBase.getINSTANCE().addList(list1);
        List<ChooserItem> list2 = new ArrayList<>();
        list2.add(new ChooserItem("Liga Włoska", "https://vignette.wikia.nocookie.net/logopedia/images/1/12/Serie_A_2019.svg/revision/latest/scale-to-width-down/140?cb=20190710115458", ChooserItem.LEAGUE,
                false, 3));
        list2.add(new ChooserItem("Liga Francuska", "https://cdn.bleacherreport.net/images/team_logos/328x328/ligue_1.png",
                ChooserItem.LEAGUE, false, 5));
        list2.add(new ChooserItem("Liga Angielska", "https://i.pinimg.com/originals/eb/83/d1/eb83d147be7b1ccfbe98247d009b3e8d.png", ChooserItem.LEAGUE, false, 6));
        TmpDataBase.getINSTANCE().addList(list2);
        List<ChooserItem> list3 = new ArrayList<>();
        list3.add(new ChooserItem("SSC Napoli", "https://upload.wikimedia.org/wikipedia/commons/thumb/2/28/S.S.C._Napoli_logo.svg/1200px-S.S.C._Napoli_logo.svg.png", ChooserItem.TEAM,
                true, 0));
        list3.add(new ChooserItem("Juventus", "https://s3-eu-central-1.amazonaws.com/centaur-wp/designweek/prod/content/uploads/2017/01/17103153/170115_logoprimario_rgb.png",
                ChooserItem.TEAM, true, 0));
        list3.add(new ChooserItem("Inter", "https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/FC_Internazionale_Milano_2014.svg/1200px-FC_Internazionale_Milano_2014.svg.png", ChooserItem.TEAM, true, 0));
        TmpDataBase.getINSTANCE().addList(list3);

    }

    @Override
    public Class<AddNewsViewModel> getViewModel() {
        return AddNewsViewModel.class;
    }

    @Override
    public int getIdFragmentContainer() {
        return R.id.add_news_container;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_add_news;
    }

    @Override
    public Navigator getNavigator() {
        return navigator;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public FragmentManager getManager() {
        return getSupportFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return binding;
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(getIdFragmentContainer());
        return ((BaseFragment) fragment).binding;
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (getCurrentFragment() instanceof BaseFragment)
            switch (((BaseFragment) getCurrentFragment()).getHomeTypeButton()) {
                case 0:
                    onBackPressed();
                    refreshHomeButton();
                    break;
                case 1:
                    return false;

            }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
    }
}
