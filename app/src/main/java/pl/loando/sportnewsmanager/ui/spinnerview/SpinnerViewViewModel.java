package pl.loando.sportnewsmanager.ui.spinnerview;

import androidx.databinding.ObservableField;

import java.util.HashMap;

import pl.loando.sportnewsmanager.adapters.SpinnerItemsAdapter;
import pl.loando.sportnewsmanager.base.BaseActivity;
import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.helpers.TmpSettingsDB;
import pl.loando.sportnewsmanager.models.SpinnerItem;

public class SpinnerViewViewModel extends BaseViewModel {
    public ObservableField<SpinnerItemsAdapter> adapter = new ObservableField();
    public ObservableField<String> title = new ObservableField<>();
    private SpinnerItemsAdapter itemsAdapter = new SpinnerItemsAdapter();

    public void init(String item) {
        TmpSettingsDB tmpSettingsDB = new TmpSettingsDB(getActivity().getApplicationContext());
        title.set(item);
        adapter.set(itemsAdapter);
        itemsAdapter.setSpinnerItems(tmpSettingsDB.getList(item));
        itemsAdapter.setTitle(item);
        itemsAdapter.setActivity((BaseActivity) getActivity());
    }
}
