package pl.loando.sportnewsmanager.ui.yournews;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.adapters.MyNewsAdapter;
import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.models.News;
import pl.loando.sportnewsmanager.models.Team;

public class YourNewsViewModel extends BaseViewModel {

    public ObservableField<RecyclerView.Adapter>adapter=new ObservableField<>();
    public ObservableInt color = new ObservableInt();
    private MyNewsAdapter myNewsAdapter = new MyNewsAdapter();
    private List<News> news = new ArrayList<>();
    public void init() {
        adapter.set(myNewsAdapter);
        color.set(R.color.colorDarkGreen);
        news.clear();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        news.add(new News("Football Italia", "https://www.football-italia.net","https://www.football-italia.net/sites/all/themes/italia/logo2.png",
                "Ancelotti: 'Di Lorenzo a surprise'","https://www.football-italia.net/sites/default/files/imagecache/main_photo/[type]/[nid]/Ancelotti-1909-occhio-epa_0.jpg",
                "https://www.football-italia.net/144718/ancelotti-di-lorenzo-surprise", date));
        news.add(new News("Transfery.info", "https://transfery.info","https://transfery.info/img/logo/logo.webp",
                "Piątek pod ostrzałem. Media i kibice krytykują","https://transfery.info/img/photos/75224/piatek-piatek.webp",
                "https://transfery.info/aktualnosci/piatek-pod-ostrzalem-media-i-kibice-krytykuja/129208", date));
        news.add(new News("Football Italia", "https://www.football-italia.net","https://www.football-italia.net/sites/all/themes/italia/logo2.png",
                "Ancelotti: 'Di Lorenzo a surprise'","https://www.football-italia.net/sites/default/files/imagecache/main_photo/[type]/[nid]/Ancelotti-1909-occhio-epa_0.jpg",
                "https://www.football-italia.net/144718/ancelotti-di-lorenzo-surprise", date));
        news.add(new News("Transfery.info", "https://transfery.info","https://transfery.info/img/logo/logo.webp",
                "Piątek pod ostrzałem. Media i kibice krytykują","https://transfery.info/img/photos/75224/piatek-piatek.webp",
                "https://transfery.info/aktualnosci/piatek-pod-ostrzalem-media-i-kibice-krytykuja/129208", date));
        news.add(new News("Football Italia", "https://www.football-italia.net","https://www.football-italia.net/sites/all/themes/italia/logo2.png",
                "Ancelotti: 'Di Lorenzo a surprise'","https://www.football-italia.net/sites/default/files/imagecache/main_photo/[type]/[nid]/Ancelotti-1909-occhio-epa_0.jpg",
                "https://www.football-italia.net/144718/ancelotti-di-lorenzo-surprise", date));
        news.add(new News("Transfery.info", "https://transfery.info","https://transfery.info/img/logo/logo.webp",
                "Piątek pod ostrzałem. Media i kibice krytykują","https://transfery.info/img/photos/75224/piatek-piatek.webp",
                "https://transfery.info/aktualnosci/piatek-pod-ostrzalem-media-i-kibice-krytykuja/129208", date));
        myNewsAdapter.setNavigator(getNavigator());
        myNewsAdapter.setActivityBinding(getBinding());
        myNewsAdapter.setNews(news);

    }
}
