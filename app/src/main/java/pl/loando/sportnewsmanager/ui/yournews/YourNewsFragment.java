package pl.loando.sportnewsmanager.ui.yournews;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.main.MainActivity;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.YourNewsFragmentBinding;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class YourNewsFragment extends BaseFragment<YourNewsFragmentBinding, YourNewsViewModel> {
    public static final String TAG = "YourNewsFragment";

    public static YourNewsFragment newInstance() {
        Bundle args = new Bundle();
        YourNewsFragment fragment = new YourNewsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public void bindData(YourNewsFragmentBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        viewModel.init();
    }

    @Override
    protected Class<YourNewsViewModel> getViewModelClass() {
        return YourNewsViewModel.class;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.your_news_fragment;
    }

    @Override
    public int getHomeTypeButton() {
        return 1;
    }

    @Override
    public int getBackPressType() {
        return 0;
    }

    @Override
    public Navigator getNavigator() {
        return ((MainActivity) getActivity()).navigator;
    }

    @Override
    public FragmentManager getManager() {
        return getFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return ((MainActivity) getActivity()).binding;
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        return binding;
    }
}
