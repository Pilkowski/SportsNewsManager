package pl.loando.sportnewsmanager.ui.newsmanagement.newsmanagement;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.recyclerview.widget.ItemTouchHelper;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.adapters.TeamsManagementListAdapter;
import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.databinding.NewsManagementBinding;
import pl.loando.sportnewsmanager.helpers.ItemTouchController;
import pl.loando.sportnewsmanager.interfaces.ItemsListener;
import pl.loando.sportnewsmanager.models.Team;
import pl.loando.sportnewsmanager.models.UserPreferences;

public class NewsManagementViewModel extends BaseViewModel implements ItemsListener {
    public ObservableField<TeamsManagementListAdapter> adapter = new ObservableField<>();
    public ObservableInt show = new ObservableInt();
    public ObservableBoolean isSet = new ObservableBoolean();
    public ObservableInt animation = new ObservableInt();
    private TeamsManagementListAdapter listAdapter = new TeamsManagementListAdapter();
    private List<Team> teams = new ArrayList<>();
    public int amount;
    public void init() {
        adapter.set(listAdapter);
        ItemTouchHelper.Callback callback = new ItemTouchController(listAdapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(((NewsManagementBinding) getFragmentBinding()).sitesList);
        ArrayList<Team> savedTeams = UserPreferences.get().getValues(UserPreferences.TEAMS,new TypeToken<ArrayList<Team>>(){});
        if (savedTeams != null)
            teams.addAll(savedTeams);
        amount = teams.size();
        updateView(amount);
    }
    public void updateView(Integer amount){
        if (amount > 0) {
            teams.clear();
            teams = UserPreferences.get().getValues(UserPreferences.TEAMS,new TypeToken<ArrayList<Team>>(){});
            listAdapter.setTeams(teams);
            listAdapter.setListener(this);
            show.set(1);
            isSet.set(false);
            animation.set(R.anim.shake);
        } else {
            show.set(0);
            isSet.set(true);
            animation.set(R.anim.shake);
        }
    }

    @Override
    public void onRemoved() {
        updateView(amount-=1);
    }
}
