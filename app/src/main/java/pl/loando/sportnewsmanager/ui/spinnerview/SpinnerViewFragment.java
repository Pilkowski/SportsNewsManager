package pl.loando.sportnewsmanager.ui.spinnerview;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.settings.SettingsActivity;
import pl.loando.sportnewsmanager.adapters.SpinnerItemsAdapter;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.SpinnerViewFragmentBinding;
import pl.loando.sportnewsmanager.models.SpinnerItem;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class SpinnerViewFragment extends BaseFragment<SpinnerViewFragmentBinding, SpinnerViewViewModel> {

    public static final String TAG = "SpinnerViewFragment";

    public static SpinnerViewFragment newInstance(String item) {

        Bundle args = new Bundle();
        SpinnerViewFragment fragment = new SpinnerViewFragment();
        args.putString("item",item);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public void bindData(SpinnerViewFragmentBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        viewModel.init(getArguments().getString("item"));
    }

    @Override
    protected Class<SpinnerViewViewModel> getViewModelClass() {
        return SpinnerViewViewModel.class;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.spinner_view_fragment;
    }

    @Override
    public int getHomeTypeButton() {
        return 0;
    }

    @Override
    public int getBackPressType() {
        return 0;
    }

    @Override
    public Navigator getNavigator() {
        return ((SettingsActivity)getActivity()).navigator;
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return ((SettingsActivity)getActivity()).binding;
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        return binding;
    }
}
