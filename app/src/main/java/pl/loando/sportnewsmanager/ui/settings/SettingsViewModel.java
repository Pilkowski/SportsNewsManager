package pl.loando.sportnewsmanager.ui.settings;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.CompoundButton;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import com.google.gson.reflect.TypeToken;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.helpers.TmpSettingsDB;
import pl.loando.sportnewsmanager.models.SpinnerItem;
import pl.loando.sportnewsmanager.models.UserPreferences;

public class SettingsViewModel extends BaseViewModel {
    public ObservableField<String> languageText = new ObservableField<>();
    public ObservableField<Drawable> languageIcon = new ObservableField<>();
    public ObservableField<String> orderText = new ObservableField<>();
    public ObservableField<String> overlapText = new ObservableField<>();
    public ObservableBoolean darkModeState = new ObservableBoolean();
    public ObservableBoolean notificationState = new ObservableBoolean();
    public ObservableField<String> showText = new ObservableField<>();
    public ObservableBoolean proposedSitesState = new ObservableBoolean();
    private TmpSettingsDB tmpSettingsDB;

    public void init(Context context) {
        tmpSettingsDB = new TmpSettingsDB(context);
        setLanguageField(context);
        setOrderField();
        setOverlapField();
        setDarkModeField();
        setNotificationField();
        setProposedSitesField();
        setShow();
    }

    private void setOrderField() {
        if (UserPreferences.get().getValues(UserPreferences.ORDERS, new TypeToken<String>(){}) != null) {
            String item = UserPreferences.get().getValues(UserPreferences.ORDERS,new TypeToken<String>(){});
            orderText.set(item);
        } else {
            SpinnerItem item = tmpSettingsDB.getOrders().get(0);
            orderText.set(item.getText());
        }
    }

    private void setLanguageField(Context context) {
        if (UserPreferences.get().getValues(UserPreferences.LANGUAGE,new TypeToken<String>(){}) != null) {
            String language = UserPreferences.get().getValues(UserPreferences.LANGUAGE,new TypeToken<String>(){});
            languageText.set(language);
            languageIcon.set(context.getDrawable(getIconRes(context, language)));

        } else {
            SpinnerItem spinnerItem = tmpSettingsDB.getLanguages().get(3);
            languageText.set(spinnerItem.getText());
            languageIcon.set(context.getDrawable(spinnerItem.getIconRes()));
        }
    }

    private int getIconRes(Context context, String language) {
            if(language.equals(context.getString(R.string.polish)))
                return R.drawable.ic_poland;
            if(language.equals(context.getString(R.string.german)))
                return R.drawable.ic_germany;
            if(language.equals(context.getString(R.string.italian)))
                return R.drawable.ic_italy;
            if(language.equals(context.getString(R.string.spanish)))
                return R.drawable.ic_spain;
            if(language.equals(context.getString(R.string.french)))
                return R.drawable.ic_france;
            if (language.equals(context.getString(R.string.english)))
                return R.drawable.ic_united_kingdom;
            return 0;
    }

    private void setShow() {
        if (UserPreferences.get().getValues(UserPreferences.SHOW,new TypeToken<String>(){}) != null) {
            String item = UserPreferences.get().getValues(UserPreferences.SHOW,new TypeToken<String>(){});
            showText.set(item);
        } else {
            SpinnerItem item = tmpSettingsDB.getShow().get(0);
            showText.set(item.getText());
        }
    }

    private void setOverlapField() {
        if (UserPreferences.get().getValues(UserPreferences.OVERLAP,new TypeToken<String>(){}) != null) {
            String item = UserPreferences.get().getValues(UserPreferences.OVERLAP,new TypeToken<String>(){});
            overlapText.set(item);
        } else {
            SpinnerItem item = tmpSettingsDB.getOverlaps().get(0);
            overlapText.set(item.getText());
        }
    }

    private void setDarkModeField() {
        if (UserPreferences.get().getValues(UserPreferences.DARK_MODE,new TypeToken<String>(){}) != null) {
            Boolean state = UserPreferences.get().getValues(UserPreferences.DARK_MODE,new TypeToken<Boolean>(){});
            darkModeState.set(state);
        } else {
            darkModeState.set(false);
        }
    }

    private void setNotificationField() {
        if (UserPreferences.get().getValues(UserPreferences.NOTIFICATIONS,new TypeToken<String>(){}) != null) {
            Boolean state = UserPreferences.get().getValues(UserPreferences.NOTIFICATIONS,new TypeToken<Boolean>(){});
            notificationState.set(state);
        } else {
            notificationState.set(false);
        }
    }

    private void setProposedSitesField() {
        if (UserPreferences.get().getValues(UserPreferences.PROPOSED_SITES,new TypeToken<String>(){}) != null) {
            Boolean state = UserPreferences.get().getValues(UserPreferences.PROPOSED_SITES,new TypeToken<Boolean>(){});
            proposedSitesState.set(state);
        } else {
            proposedSitesState.set(false);
        }
    }

    public void onLanguageClick() {
        getNavigator().openSpinnerItemView(UserPreferences.LANGUAGE);
    }

    public void onShowClick() {
        getNavigator().openSpinnerItemView(UserPreferences.SHOW);
    }

    public void onOrderClick() {
        getNavigator().openSpinnerItemView(UserPreferences.ORDERS);
    }

    public void onOverlapClick() {
        getNavigator().openSpinnerItemView(UserPreferences.OVERLAP);
    }

    @SuppressLint("ResourceType")
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton.getId() == 2131361880) {
            UserPreferences.get().save(UserPreferences.DARK_MODE, b);
        }
        if (compoundButton.getId() == 2131361881) {
            UserPreferences.get().save(UserPreferences.NOTIFICATIONS, b);
        }
        if (compoundButton.getId() == 2131361882) {
            UserPreferences.get().save(UserPreferences.PROPOSED_SITES, b);
        }
    }
}
