package pl.loando.sportnewsmanager.ui.newsmanagement.newsmanagement;

import android.os.Bundle;
import android.util.Log;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.main.MainActivity;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.NewsManagementBinding;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class NewsManagementFragment extends BaseFragment<NewsManagementBinding, NewsManagementViewModel> {
    public static final String TAG = "NewsManagementFragment";

    public static NewsManagementFragment newInstance() {
        Bundle args = new Bundle();
        NewsManagementFragment fragment = new NewsManagementFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public void bindData(NewsManagementBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        viewModel.init();
    }

    @Override
    protected Class<NewsManagementViewModel> getViewModelClass() {
        return NewsManagementViewModel.class;
    }

    public void addTeam(){
        viewModel.updateView(++viewModel.amount);
    }
    @Override
    public int getLayoutRes() {
        return R.layout.news_management;
    }

    @Override
    public int getHomeTypeButton() {
        return 1;
    }

    @Override
    public int getBackPressType() {
        return 1;
    }

    @Override
    public Navigator getNavigator() {
        return ((MainActivity) getActivity()).navigator;
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return ((MainActivity) getActivity()).binding;
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        return binding;
    }
}
