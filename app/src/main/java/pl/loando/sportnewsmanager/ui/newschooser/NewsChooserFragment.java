package pl.loando.sportnewsmanager.ui.newschooser;

import android.os.Bundle;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.addnews.AddNewsActivity;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.NewsChooserFragmentBinding;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class NewsChooserFragment extends BaseFragment<NewsChooserFragmentBinding, NewsChooserViewModel> {

    public static final String TAG = "NewsChooserFragment";

    public static NewsChooserFragment newInstance(int navigateToId) {

        Bundle args = new Bundle();
        args.putInt("id", navigateToId);
        NewsChooserFragment fragment = new NewsChooserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public void bindData(NewsChooserFragmentBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        viewModel.init(getArguments().getInt("id"));
    }

    @Override
    protected Class<NewsChooserViewModel> getViewModelClass() {
        return NewsChooserViewModel.class;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.news_chooser_fragment;
    }

    @Override
    public int getHomeTypeButton() {
        return 0;
    }

    @Override
    public int getBackPressType() {
        return 0;
    }

    @Override
    public Navigator getNavigator() {
        return ((AddNewsActivity) getActivity()).navigator;
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return ((AddNewsActivity) getActivity()).binding;
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        return binding;
    }
}
