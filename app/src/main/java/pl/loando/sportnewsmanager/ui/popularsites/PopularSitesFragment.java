package pl.loando.sportnewsmanager.ui.popularsites;

import android.util.Log;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.main.MainActivity;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.PopularSitesFragmentBinding;
import pl.loando.sportnewsmanager.interfaces.Providers;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class PopularSitesFragment extends BaseFragment<PopularSitesFragmentBinding, PopularSitesViewModel> implements Providers {
    public static final String TAG = "PopularSitesFragment";

    public static PopularSitesFragment newInstance() {
        return new PopularSitesFragment();
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public void bindData(PopularSitesFragmentBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        viewModel.init();
    }

    @Override
    protected Class<PopularSitesViewModel> getViewModelClass() {
        return PopularSitesViewModel.class;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.popular_sites_fragment;
    }

    @Override
    public int getHomeTypeButton() {
        return 1;
    }

    @Override
    public int getBackPressType() {
        return 1;
    }

    @Override
    public Navigator getNavigator() {
        return ((MainActivity) getActivity()).getNavigator();
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return ((MainActivity) getActivity()).getBinding();
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        return ((MainActivity) getActivity()).getFragmentBinding();
    }
}
