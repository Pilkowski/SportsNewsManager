package pl.loando.sportnewsmanager.ui.home;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

import android.util.Log;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.activities.main.MainActivity;
import pl.loando.sportnewsmanager.adapters.ViewPagerListAdapter;
import pl.loando.sportnewsmanager.base.BaseFragment;
import pl.loando.sportnewsmanager.databinding.HomeFragmentBinding;
import pl.loando.sportnewsmanager.interfaces.Providers;
import pl.loando.sportnewsmanager.navigation.Navigator;
import pl.loando.sportnewsmanager.ui.newsmanagement.newsmanagement.NewsManagementFragment;

public class HomeFragment extends BaseFragment<HomeFragmentBinding, HomeViewModel> implements Providers {

    public static final String TAG = "HomeFragment";

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getFragmentTag() {
        return TAG;
    }

    @Override
    public void bindData(HomeFragmentBinding binding) {
        binding.setViewModel(viewModel);
        viewModel.setProviders(this);
        viewModel.init();
    }

    public void addTeam(){
        if (binding.viewPager.getAdapter() != null)
            for (BaseFragment fragment :
                    ((ViewPagerListAdapter) binding.viewPager.getAdapter()).getFragments()) {
                if (fragment.getFragmentTag().equals(NewsManagementFragment.TAG)) {
                    ((NewsManagementFragment) fragment).addTeam();
                }
            }
    }

    @Override
    protected Class getViewModelClass() {
        return HomeViewModel.class;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.home_fragment;
    }

    @Override
    public int getHomeTypeButton() {
        return 1;
    }

    @Override
    public int getBackPressType() {
        return 0;
    }


    @Override
    public Navigator getNavigator() {
        return ((MainActivity) getActivity()).getNavigator();
    }

    @Override
    public FragmentManager getManager() {
        return getChildFragmentManager();
    }

    @Override
    public ViewDataBinding getBinding() {
        return ((MainActivity) getActivity()).getBinding();
    }

    @Override
    public ViewDataBinding getFragmentBinding() {
        return ((MainActivity) getActivity()).getFragmentBinding();
    }
}
