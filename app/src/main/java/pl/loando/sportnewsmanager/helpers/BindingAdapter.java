package pl.loando.sportnewsmanager.helpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.adapters.PopularSitesListAdapter;
import pl.loando.sportnewsmanager.databinding.ActivityMainBinding;
import pl.loando.sportnewsmanager.databinding.HomeFragmentBinding;
import pl.loando.sportnewsmanager.models.ChooserItem;
import pl.loando.sportnewsmanager.models.Site;

public class BindingAdapter {
    public static final String TAG = "Binding Adapter";
    private static MenuItem prevBotNavMenuItem;

    public static int getSelector(int itemId) {
        switch (itemId) {
            case R.id.nav_your_news:
                return R.drawable.bot_nav_news_icon_selector;
            case R.id.nav_add_news:
                return R.drawable.bot_nav_add_icon_selector;
            case R.id.nav_popular_sites:
                return R.drawable.bot_nav_popular_icon_selector;
        }
        return itemId;
    }

    private static int getMenuItemPosition(int itemId) {
        switch (itemId) {
            case R.id.nav_your_news:
                return 0;
            case R.id.nav_add_news:
                return 1;
            case R.id.nav_popular_sites:
                return 2;
        }
        return itemId;
    }

    @androidx.databinding.BindingAdapter({"attachViewPager"})
    public static void setOnNavigationItemSelected(final BottomNavigationView bottomNavigationView, final ViewDataBinding binding) {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_your_news:
                        ((HomeFragmentBinding) binding).viewPager.setCurrentItem(0);
                        break;
                    case R.id.nav_add_news:
                        ((HomeFragmentBinding) binding).viewPager.setCurrentItem(1);
                        break;
                    case R.id.nav_popular_sites:
                        ((HomeFragmentBinding) binding).viewPager.setCurrentItem(2);
                        break;
                    default:
                        return true;
                }
                return false;
            }

        });
    }

    @androidx.databinding.BindingAdapter("viewPagerAdapter")
    public static void setViewPagerAdapter(ViewPager viewPager, FragmentPagerAdapter fragmentPagerAdapter) {
        viewPager.setAdapter(fragmentPagerAdapter);
    }

    @androidx.databinding.BindingAdapter("attachBottomNavigationView")
    public static void setOnPageChanged(final ViewPager viewPager, final ViewDataBinding binding) {

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            StateListDrawable stateListDrawable;
            int[] state;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Animation animation = AnimationUtils.loadAnimation(viewPager.getContext(), R.anim.fade_out_fade_in);
                if (prevBotNavMenuItem != null) {
                    ((ActivityMainBinding) binding).bottomNavigationView.getMenu().getItem(getMenuItemPosition(prevBotNavMenuItem.getItemId())).setChecked(false);
                    stateListDrawable = (StateListDrawable) ((ActivityMainBinding) binding).bottomNavigationView.getContext().getDrawable(getSelector(prevBotNavMenuItem.getItemId()));
                    state = new int[]{!((ActivityMainBinding) binding).bottomNavigationView.getMenu().getItem(getMenuItemPosition(prevBotNavMenuItem.getItemId())).isChecked() ? android.R.attr.state_checked : -android.R.attr.state_checked};
                    stateListDrawable.setState(state);
                    ((ActivityMainBinding) binding).bottomNavigationView.getMenu().getItem(getMenuItemPosition(prevBotNavMenuItem.getItemId())).setIcon(stateListDrawable.getCurrent());
                    ((ActivityMainBinding) binding).fab.startAnimation(animation);
                }
                ((ActivityMainBinding) binding).bottomNavigationView.getMenu().getItem(position).setChecked(true);
                stateListDrawable = (StateListDrawable) ((ActivityMainBinding) binding).bottomNavigationView.getContext().getDrawable(getSelector(((ActivityMainBinding) binding).bottomNavigationView.getMenu().getItem(position).getItemId()));
                state = new int[]{((ActivityMainBinding) binding).bottomNavigationView.getMenu().getItem(position).isChecked() ? android.R.attr.state_checked : -android.R.attr.state_checked};
                stateListDrawable.setState(state);
                ((ActivityMainBinding) binding).bottomNavigationView.getMenu().getItem(position).setIcon(stateListDrawable.getCurrent());
                prevBotNavMenuItem = ((ActivityMainBinding) binding).bottomNavigationView.getMenu().getItem(position);
                ((ActivityMainBinding) binding).fab.startAnimation(animation);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @androidx.databinding.BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();

        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

    }

    @androidx.databinding.BindingAdapter("recyclerView_adapter")
    public static void setRecyclerViewAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @androidx.databinding.BindingAdapter("set_layoutManager")
    public static void setLayoutManager(RecyclerView recyclerView, Context context) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    @androidx.databinding.BindingAdapter("onRefresh")
    public static void setOnRefreshListener(final SwipeRefreshLayout swipeRefreshLayout, final RecyclerView.Adapter adapter) {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter instanceof PopularSitesListAdapter) {
                    ((PopularSitesListAdapter) adapter).refresh();
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @androidx.databinding.BindingAdapter("setColorScheme")
    public static void setColorScheme(SwipeRefreshLayout swipeRefreshLayout, int color) {
        swipeRefreshLayout.setColorSchemeResources(color);
    }

    @androidx.databinding.BindingAdapter("showView")
    public static void showView(View view, int show) {
        view.setVisibility(show == 1 ? View.VISIBLE : View.GONE);
    }

    @androidx.databinding.BindingAdapter("setFadeInAnimation")
    public static void showFadeInAnimation(final View view, boolean isSet) {
        if (isSet) {
            view.setVisibility(View.VISIBLE);
            view.animate()
                    .alpha(1.0f)
                    .setDuration(550)
                    .setListener(null);
        }
        else{
            view.setAlpha(0f);
            view.setVisibility(View.GONE);
        }
    }

    @androidx.databinding.BindingAdapter("setOnShowLabelDrawable")
    public static void setOnShowLabelDrawable(ImageView imageView, int show) {
        Animation animation;
        switch (show) {
            case 1:
                animation = AnimationUtils.loadAnimation(imageView.getContext(), R.anim.rotate_0_to_180);
                imageView.startAnimation(animation);
                break;
            case 0:
                animation = AnimationUtils.loadAnimation(imageView.getContext(), R.anim.rotate_180_to_360);
                imageView.startAnimation(animation);
                break;
            case -1:
                break;
        }

    }

    @androidx.databinding.BindingAdapter("place")
    public static void setPlaceText(TextView textView, int place) {
        Context context = textView.getContext();
        switch (place) {
            case Site.FIRST:
                textView.setText(context.getString(R.string.most_popular_site));
                textView.setTextSize(13);
                break;
            case Site.SECOND:
                textView.setText(context.getString(R.string.second_popular_site));
                textView.setTextSize(13);
                break;
            case Site.THIRD:
                textView.setText(context.getString(R.string.third_popular_site));
                textView.setTextSize(15);
                break;
            default:
                break;
        }
    }

    @androidx.databinding.BindingAdapter("showFab")
    public static void showFab(FloatingActionButton fab, boolean show) {
        if (show) {
            fab.show();
        } else {
            fab.hide();
        }
    }

    @androidx.databinding.BindingAdapter("newsChooserLabelText")
    public static void setNewsChooserLabelText(TextView textView, String type) {
        switch (type) {
            case ChooserItem.DISCIPLINE:
                textView.setText(textView.getContext().getString(R.string.type_dicipline));
            case ChooserItem.LEAGUE:
                textView.setText(textView.getContext().getString(R.string.type_league));
            case ChooserItem.TEAM:
                textView.setText(textView.getContext().getString(R.string.type_team));
        }
    }

    @androidx.databinding.BindingAdapter("setTextColor")
    public static void setTextColor(TextView view, boolean read) {
        if (read)
            view.setTextColor(view.getContext().getColor(R.color.colorBlack));
        else
            view.setTextColor(view.getContext().getColor(R.color.colorDarkGray));
    }

    @androidx.databinding.BindingAdapter("setAnimation")
    public static void setAnim(final View view, int animId) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), animId);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                view.startAnimation(animation);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                onAnimationRepeat(animation);

            }

            @Override
            public void onAnimationRepeat(final Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onAnimationStart(animation);
                    }
                }, 750);
            }
        });
        view.startAnimation(animation);
    }
}
