package pl.loando.sportnewsmanager.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.databinding.SingleSiteManagementBinding;
import pl.loando.sportnewsmanager.interfaces.ItemTouchHelperListener;
import pl.loando.sportnewsmanager.interfaces.ItemsListener;
import pl.loando.sportnewsmanager.interfaces.RecyclerViewListener;
import pl.loando.sportnewsmanager.models.Team;
import pl.loando.sportnewsmanager.models.UserPreferences;

public class TeamsManagementListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemTouchHelperListener, RecyclerViewListener{
    private List<Team>teams=new ArrayList<>();
    public static final int HEADER = 0;
    public static final int TEAM = 1;
    private ItemsListener listener;
    private final String TAG = "TeamsManagement";

    public void setTeams(List<Team> teams) {
        this.teams.clear();
        this.teams.addAll(teams);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        if(viewType==HEADER)
        {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_management_header,parent,false);
            return new TeamsHeaderViewHolder(itemView);
        }else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_site_management, parent, false);
            SingleSiteManagementBinding binding = SingleSiteManagementBinding.bind(itemView);
            TeamsManagementAdapterViewModel viewModel = new TeamsManagementAdapterViewModel();
            binding.setViewModel(viewModel);
            return new TeamsViewHolder(itemView, binding, viewModel, this);
        }
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if(position==0)
            return;
        ((TeamsViewHolder) holder).setElement(teams.get(position-1));
    }
    @Override
    public int getItemCount() {
        return teams.size()+1;
    }

    @Override
    public void onRemoveItem(int position) {
        UserPreferences.get().removeTeam(UserPreferences.TEAMS, position-1,new TypeToken<ArrayList<Team>>(){});
        teams.remove(position-1);
        notifyItemRemoved(position);
        listener.onRemoved();
    }

    @Override
    public int getItemViewType(int position) {
        if(position==0)
            return HEADER;
        return TEAM;
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                if (teams.size() < i + 1)
                    Collections.swap(teams, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                if (teams.size() < i)
                    Collections.swap(teams, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    public void setListener(ItemsListener listener) {
        this.listener = listener;
    }

    private class TeamsHeaderViewHolder extends RecyclerView.ViewHolder{

        public TeamsHeaderViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
    private class TeamsViewHolder extends RecyclerView.ViewHolder{
        private SingleSiteManagementBinding binding;
        private TeamsManagementAdapterViewModel viewModel;
        private RecyclerViewListener listener;
        public TeamsViewHolder(@NonNull View itemView, SingleSiteManagementBinding binding, TeamsManagementAdapterViewModel viewModel, final RecyclerViewListener listener) {
            super(itemView);
            this.binding = binding;
            this.viewModel = viewModel;
            this.listener = listener;
            binding.singleSiteManagementRemoveItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onRemoveItem(getAdapterPosition());
                }
            });
        }
        public void setElement(Team team){
            viewModel.initTeam(team);
        }

    }

}
