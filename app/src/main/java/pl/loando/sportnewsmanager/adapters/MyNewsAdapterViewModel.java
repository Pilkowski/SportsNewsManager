package pl.loando.sportnewsmanager.adapters;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.interfaces.BadgeListener;
import pl.loando.sportnewsmanager.models.News;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class MyNewsAdapterViewModel extends BaseViewModel {
    public ObservableField<String> srcName = new ObservableField<>();
    public ObservableField<String> srcUrl = new ObservableField<>();
    public ObservableField<String> newsTitle = new ObservableField<>();
    public ObservableField<String> newsUrl = new ObservableField<>();
    public ObservableField<String> date = new ObservableField<>();
    public ObservableBoolean textColor = new ObservableBoolean();
    private News news;
    private Navigator navigator;
    private BadgeListener listener;

    public void setListener(BadgeListener listener) {
        this.listener = listener;
    }

    public void init(News news, Navigator navigator) {
        this.news = news;
        this.navigator = navigator;
        srcName.set(news.getSrcName());
        srcUrl.set(news.getSrcUrl());
        newsTitle.set(news.getNewsTitle());
        newsUrl.set(news.getNewsUrl());
        textColor.set(true);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        date.set("Dodano " + dateFormat.format(news.getDate()));
    }

    public void onSrcClick() {
        navigator.openSite(news.getSrcUri());
    }

    public void onNewsClick() {
        navigator.openSite(news.getNewsUri());
        if (textColor.get()) {
            listener.setBadgeValue(MyNewsAdapter.TYPE_NEWS);
            textColor.set(false);
        }
    }

}
