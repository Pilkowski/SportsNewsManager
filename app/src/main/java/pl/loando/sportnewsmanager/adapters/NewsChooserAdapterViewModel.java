package pl.loando.sportnewsmanager.adapters;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import pl.loando.sportnewsmanager.base.BaseViewModel;
import pl.loando.sportnewsmanager.models.ChooserItem;
import pl.loando.sportnewsmanager.models.Team;
import pl.loando.sportnewsmanager.models.UserPreferences;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class NewsChooserAdapterViewModel extends BaseViewModel {
    public ObservableField<String> name = new ObservableField<>();
    public ObservableInt showNextIcon = new ObservableInt();
    public ObservableField<String> imageUrl = new ObservableField<>();
    public ObservableField<String> label = new ObservableField<>();
    private ChooserItem item;
    private Navigator navigator;
    private Activity activity;
    public void init(ChooserItem item, Navigator navigator, Activity activity){
        this.item=item;
        this.navigator=navigator;
        this.activity = activity;
        name.set(item.getName());
        imageUrl.set(item.getItemUrl());
        if(item.isEnabled())
            showNextIcon.set(PopularSitesListAdapterViewModel.GONE);
        else showNextIcon.set(PopularSitesListAdapterViewModel.VISIBLE);
    }
    public void onClick(){
        if(!item.isEnabled()) {
            navigator.showNewsChooserContent(item.getNavigateToId());
        }
        else {
            Toast.makeText(activity.getApplicationContext(), "Dodano drużynę !!!", Toast.LENGTH_SHORT).show();
            UserPreferences.get().addTeam(UserPreferences.TEAMS, new Team(item.getName(),item.getItemUrl()),new TypeToken<ArrayList<Team>>(){});
            Intent returnIntent = new Intent();
            activity.setResult(Activity.RESULT_OK, returnIntent);
            activity.finish();
        }
    }
    void setTitleLabel(String type){
        label.set(type);
    }
}
