package pl.loando.sportnewsmanager.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.databinding.NewsChooserHeaderBinding;
import pl.loando.sportnewsmanager.databinding.NewsChooserSingleItemBinding;
import pl.loando.sportnewsmanager.models.ChooserItem;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class NewsChooserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER = 0;
    private static final int ITEM = 1;
    private List<ChooserItem> items = new ArrayList<>();
    private Navigator navigator;
    private Activity activity;

    public void setItems(List<ChooserItem> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }
    public void setNavigator(Navigator navigator){
        this.navigator=navigator;
    }

    public void setActivity(Activity activity){
        this.activity=activity;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        ViewDataBinding binding;
        NewsChooserAdapterViewModel viewModel = new NewsChooserAdapterViewModel();
        switch (viewType) {
            case HEADER:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_chooser_header, parent, false);
                binding = NewsChooserHeaderBinding.bind(itemView);
                ((NewsChooserHeaderBinding) binding).setViewModel(viewModel);
                return new NewsChooserHeaderViewHolder(itemView, (NewsChooserHeaderBinding) binding, viewModel);
            case ITEM:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_chooser_single_item, parent, false);
                binding = NewsChooserSingleItemBinding.bind(itemView);
                ((NewsChooserSingleItemBinding) binding).setViewModel(viewModel);
                return new NewsChooserSingleItemViewHolder(itemView, (NewsChooserSingleItemBinding) binding, viewModel);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            ((NewsChooserHeaderViewHolder) holder).setHeaderLabel(items.get(position).getType());
            return;
        }
        ((NewsChooserSingleItemViewHolder) holder).setElement(items.get(position - 1));
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return HEADER;
        return ITEM;
    }

    @Override
    public int getItemCount() {
        return items.size() + 1;
    }

    private class NewsChooserSingleItemViewHolder extends RecyclerView.ViewHolder {
        NewsChooserSingleItemBinding binding;
        NewsChooserAdapterViewModel viewModel;

        NewsChooserSingleItemViewHolder(@NonNull View itemView, NewsChooserSingleItemBinding binding, NewsChooserAdapterViewModel viewModel) {
            super(itemView);
            this.binding = binding;
            this.viewModel = viewModel;
        }

        void setElement(ChooserItem item) {
            viewModel.init(item, navigator, activity);
        }
    }

    private class NewsChooserHeaderViewHolder extends RecyclerView.ViewHolder {
        NewsChooserHeaderBinding binding;
        NewsChooserAdapterViewModel viewModel;

        NewsChooserHeaderViewHolder(@NonNull View itemView, NewsChooserHeaderBinding binding, NewsChooserAdapterViewModel viewModel) {
            super(itemView);
            this.binding = binding;
            this.viewModel = viewModel;
        }

        void setHeaderLabel(String type) {
            viewModel.setTitleLabel(type);
        }
    }
}
