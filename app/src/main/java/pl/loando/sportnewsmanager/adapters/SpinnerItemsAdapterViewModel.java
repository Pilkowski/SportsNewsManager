package pl.loando.sportnewsmanager.adapters;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.base.BaseActivity;
import pl.loando.sportnewsmanager.models.SpinnerItem;
import pl.loando.sportnewsmanager.models.UserPreferences;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class SpinnerItemsAdapterViewModel {

    public ObservableField<String>text = new ObservableField<>();
    public ObservableField<Drawable> icon = new ObservableField<>();
    public ObservableInt padding = new ObservableInt();
    private BaseActivity activity;
    private SpinnerItem item;
    private String title;

    public void init(SpinnerItem item, BaseActivity activity, String title){
        text.set(item.getText());
        if(item.getIconRes()!=0){
            icon.set(activity.getApplicationContext().getDrawable(item.getIconRes()));
            padding.set(35);
        }
        else {
            icon.set(activity.getApplicationContext().getDrawable(R.drawable.empty_spinner_view_background));
            padding.set(0);
        }
        this.activity = activity;
        this.item = item;
        this.title = title;
    }
    public void onClick(){
        UserPreferences.get().save(title,text.get());
        activity.onBackPressed();
    }
}
