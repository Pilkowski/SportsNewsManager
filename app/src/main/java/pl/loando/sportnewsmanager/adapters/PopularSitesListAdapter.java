package pl.loando.sportnewsmanager.adapters;

import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import pl.loando.sportnewsmanager.R;
import pl.loando.sportnewsmanager.databinding.SingleHotSiteBinding;
import pl.loando.sportnewsmanager.databinding.SingleSiteBinding;
import pl.loando.sportnewsmanager.models.Site;
import pl.loando.sportnewsmanager.navigation.Navigator;

public class PopularSitesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int POPULAR_SITE = 2;
    private static final int SITE = 3;
    private List<Site> popularSitesList = new ArrayList<>();
    private static final int FIRST_ITEM = 1;
    private Navigator navigator;

    public void setPopularSitesList(List<Site> popularSitesList) {
        this.popularSitesList.clear();
        this.popularSitesList.addAll(popularSitesList);
        notifyDataSetChanged();
    }

    public void setNavigator(Navigator navigator) {
        this.navigator = navigator;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        ViewDataBinding binding;
        PopularSitesListAdapterViewModel viewModel = new PopularSitesListAdapterViewModel();
        switch (viewType) {
            case FIRST_ITEM:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.popular_sites_header, parent, false);
                return new HeaderViewHolder(itemView);
            case POPULAR_SITE:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_hot_site, parent, false);
                binding = SingleHotSiteBinding.bind(itemView);
                ((SingleHotSiteBinding) binding).setViewModel(viewModel);
                return new PopularHotSiteViewHolder(itemView, (SingleHotSiteBinding) binding, viewModel);
            case SITE:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_site, parent, false);
                binding = SingleSiteBinding.bind(itemView);
                ((SingleSiteBinding) binding).setViewModel(viewModel);
                return new PopularSiteViewHolder(itemView, (SingleSiteBinding) binding, viewModel);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (position == 0)
            return;
        Site site = popularSitesList.get(position - 1);
        if (site != null) {
            if (site.getIsHotSite())
                ((PopularHotSiteViewHolder) holder).setElement(site, navigator);
            else
                ((PopularSiteViewHolder) holder).setElement(site, navigator);
        }
        if (position + 1 == getItemCount()) {
            setBottomMargin(holder.itemView, (int) (96 * Resources.getSystem().getDisplayMetrics().density));
        } else {
            setBottomMargin(holder.itemView, (int) (16 * Resources.getSystem().getDisplayMetrics().density));
        }
    }

    private static void setBottomMargin(View view, int bottomMargin) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, bottomMargin);
            view.requestLayout();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return FIRST_ITEM;
        else {
            if (popularSitesList.get(position - 1).getIsHotSite())
                return POPULAR_SITE;
            else
                return SITE;
        }
    }

    @Override
    public int getItemCount() {
        if (popularSitesList == null)
            return 0;
        return popularSitesList.size() + 1;
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public static class PopularHotSiteViewHolder extends RecyclerView.ViewHolder {
        SingleHotSiteBinding binding;
        PopularSitesListAdapterViewModel viewModel;

        PopularHotSiteViewHolder(@NonNull View itemView, SingleHotSiteBinding binding, PopularSitesListAdapterViewModel viewModel) {
            super(itemView);
            LinearLayout linearLayout = itemView.findViewById(R.id.single_site_item_background);
            linearLayout.setBackgroundResource(R.drawable.popular_site_background);
            AnimationDrawable animationDrawable = (AnimationDrawable) linearLayout.getBackground();
            animationDrawable.setEnterFadeDuration(1000);
            animationDrawable.setExitFadeDuration(1000);
            animationDrawable.start();
            this.binding = binding;
            this.viewModel = viewModel;
        }

        void setElement(Site site, Navigator navigator) {
            viewModel.init(site, navigator);
            viewModel.initPlace();
        }
    }

    public static class PopularSiteViewHolder extends RecyclerView.ViewHolder {
        SingleSiteBinding binding;
        PopularSitesListAdapterViewModel viewModel;

        PopularSiteViewHolder(@NonNull View itemView, SingleSiteBinding binding, PopularSitesListAdapterViewModel viewModel) {
            super(itemView);
            this.binding = binding;
            this.viewModel = viewModel;
        }

        void setElement(Site site, Navigator navigator) {
            viewModel.init(site, navigator);
        }
    }
}
