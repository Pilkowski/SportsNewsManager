package pl.loando.sportnewsmanager.models;

import java.util.Comparator;

public class Site {
    private String name;
    private String label;
    private String uri;
    private String url;
    private Boolean isHotSite;
    private int value;
    public static final int FIRST = 1;
    public static final int SECOND = 2;
    public static final int THIRD = 3;
    public static final int DEFAULT = 4;
    private int place;

    public Site(String name, String label, String uri, String url, int value) {
        this.name = name;
        this.label = label;
        this.uri = uri;
        this.url = url;
        this.value = value;
    }

    public static Comparator<Site> SITE_COMPARATOR = new Comparator<Site>() {
        @Override
        public int compare(Site site, Site nextSite) {
            return Integer.compare(nextSite.getValue(), site.getValue());
        }
    };

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public String getUri() {
        return uri;
    }

    public String getUrl() {
        return url;
    }

    public Boolean getIsHotSite() {
        return isHotSite;
    }

    public void setHotSite(Boolean hotSite) {
        isHotSite = hotSite;
    }

    private int getValue() {
        return value;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }
}
