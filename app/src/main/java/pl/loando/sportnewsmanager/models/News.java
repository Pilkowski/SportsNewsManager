package pl.loando.sportnewsmanager.models;

import java.util.Date;

public class News {
    private String srcName;
    private String srcUri;
    private String srcUrl;
    private String newsTitle;
    private String newsUrl;
    private String newsUri;
    private Date date;

    public News(String srcName, String srcUri, String srcUrl, String newsTitle,String newsUrl, String newsUri, Date date) {
        this.srcName = srcName;
        this.srcUri = srcUri;
        this.srcUrl  =srcUrl;
        this.newsTitle = newsTitle;
        this.newsUri = newsUri;
        this.newsUrl = newsUrl;
        this.date = date;
    }

    public String getSrcName() {
        return srcName;
    }

    public String getSrcUri() {
        return srcUri;
    }

    public String getNewsTitle() {
        return newsTitle;
    }


    public String getNewsUri() {
        return newsUri;
    }

    public Date getDate() {
        return date;
    }

    public String getSrcUrl() {
        return srcUrl;
    }

    public String getNewsUrl() {
        return newsUrl;
    }
}
