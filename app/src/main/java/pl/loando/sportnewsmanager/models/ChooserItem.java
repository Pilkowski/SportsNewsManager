package pl.loando.sportnewsmanager.models;

public class ChooserItem {
    public static final String DISCIPLINE="DISCIPLINE";
    public static final String LEAGUE="LEAGUE";
    public static final String TEAM="TEAM";

    private String name;
    private String itemUrl;
    private String type;
    private boolean enabled;
    private int navigateToId;
    public ChooserItem(String name, String itemUrl, String type, boolean enabled, int navigateToId) {
        this.name = name;
        this.itemUrl = itemUrl;
        this.type = type;
        this.enabled = enabled;
        this.navigateToId=navigateToId;
    }

    public String getName() {
        return name;
    }

    public String getItemUrl() {
        return itemUrl;
    }

    public String getType() {
        return type;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public int getNavigateToId() {
        return navigateToId;
    }
}
