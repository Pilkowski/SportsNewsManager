package pl.loando.sportnewsmanager.base;

import android.app.Activity;
import android.content.Context;

import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModel;

import pl.loando.sportnewsmanager.interfaces.Providers;
import pl.loando.sportnewsmanager.navigation.Navigator;

public abstract class BaseViewModel extends ViewModel {
    private Providers providers;

    public void setProviders(Providers providers) {
        this.providers = providers;
    }

    public Activity getActivity(){return providers.getActivity();}
    public Navigator getNavigator(){
       return providers.getNavigator();
    }
    public FragmentManager getManager(){
        return providers.getManager();
    }
    public ViewDataBinding getBinding(){
        return providers.getBinding();
    }
    public ViewDataBinding getFragmentBinding(){
        return providers.getFragmentBinding();
    }
}
